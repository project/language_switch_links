CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Translates the language switcher block links to appropriate native language. 

Based on the code from the following article:
https://www.axelerant.com/resources/team-blog/native-language-switcher-
multilingual-sites-drupal


REQUIREMENTS
------------

This module requires no modules outside of Drupal core. Language module must be
enabled.


INSTALLATION
------------

Install the Language Switch Links module as you would normally install a
contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

No additional configuration required. Language switcher block links are
translated whenever this module is enabled.

MAINTAINERS
-----------

 * Steven DuBois - https://www.drupal.org/u/srdtwc

Supporting organization:

 * DevCollaborative, LLC - https://devcollaborative.com
